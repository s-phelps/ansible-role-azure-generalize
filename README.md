Ansible Role Azure Generalize
=========

This automates the preparation and generalization of an instance running in Azure.

Requirements
------------

TODO

Role Variables
--------------

TODO

Dependencies
------------

Azure API credentials

Example Playbook
----------------


    - hosts: server
      roles:
          - { role: azure-generalize }

License
-------

TBD

Author Information
------------------

Scott Phelps